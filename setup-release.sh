#!/bin/sh

# Setup python3 venv
python3 -m venv env

# Activate env
. ./env/bin/activate

# Install dependencies
pip install GitPython semantic-version

