ARG FRAPPE_BRANCH=develop
FROM node:14-buster

COPY install_frappe.sh /install_frappe
COPY install_erpnext.sh /install_erpnext
COPY install_app.sh /install_app
COPY prepare_production.sh /prepare_production

# Install frappe
RUN /install_frappe && \
  # Install erpnext
  /install_erpnext && \
  # Install required apps with following command
  # /install_app <app_name> <repo> <app_branch>
  /install_app apparelo https://github.com/aerele/apparelo develop && \
  /install_app erpnext_shipping https://github.com/frappe/erpnext-shipping master && \
  /install_app pdf_on_submit https://github.com/alyf-de/erpnext_pdf-on-submit develop && \
  /install_app erpnext_customer_statements_sender https://github.com/dvdl16/erpnext_customer_statements_sender master && \
  # Cleanup for production
  /prepare_production

FROM frappe/erpnext-nginx:${FRAPPE_BRANCH}

COPY --from=0 /home/frappe/frappe-bench/sites/ /var/www/html/
COPY --from=0 /rsync /rsync

# Append list of installed to apps.txt
RUN echo -n "posawesome\nfrappe_s3_attachment\nmetabase_integration\nbookings\nbench_manager" >> /var/www/html/apps.txt

VOLUME [ "/assets" ]

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
