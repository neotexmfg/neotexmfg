### Introduction

This setup uses following apps:

- https://github.com/aerele/apparelo
- https://github.com/alyf-de/erpnext_pdf-on-submit
- https://github.com/frappe/erpnext-shipping
- https://github.com/dvdl16/erpnext_customer_statements_sender

### Build images

Execute from root of repo

For nginx:

```shell
# For version-13
docker build --build-arg=FRAPPE_BRANCH=version-13 -t registry.gitlab.com/neotexmfg/neotexmfg/erpnext-nginx:v13 nginx
```

For worker:

```shell
# For version-13
docker build --build-arg=FRAPPE_BRANCH=version-13 -t registry.gitlab.com/neotexmfg/neotexmfg/erpnext-worker:v13 worker
```
